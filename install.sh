#!/bin/bash

set -e
set -u

CF_USER=admin
CF_PASS=admin
CF_API=https://api.local.pcfdev.io
CF_ORG=pcfdev-org
CF_SPACE=pcfdev-space

cf login -a $CF_API -u $CF_USER -p $CF_PASS --skip-ssl-validation -o $CF_ORG -s $CF_SPACE

rm -rf src

cf delete -f wp 
cf delete-service -f wp_db
cf delete-service -f wp_volume ||echo "could not delete wp_volume"

cf create-service local-volume free-local-disk wp_volume
cf create-service p-mysql 512mb wp_db

# push empty app to get the mysql credentials
mkdir src
cp -r .bp-config src
cp -r .extensions src
cf push --no-start
pushd src
wp core download

# parse mysql credentials
VCAP_SERVICES=$(cf env wp | head -50 | tail -46 | jq .)
DBHOSTNAME=$(echo $VCAP_SERVICES |jq '.VCAP_SERVICES."p-mysql"[0].credentials.hostname'|tr -d '"')
USERNAME=$(echo $VCAP_SERVICES |jq '.VCAP_SERVICES."p-mysql"[0].credentials.username'|tr -d '"')
PASSWORD=$(echo $VCAP_SERVICES |jq '.VCAP_SERVICES."p-mysql"[0].credentials.password'|tr -d '"')
DBNAME=$(echo $VCAP_SERVICES |jq '.VCAP_SERVICES."p-mysql"[0].credentials.name'|tr -d '"')

# configure and install
wp core config --dbname=$DBNAME --dbuser=$USERNAME --dbpass=$PASSWORD --dbhost=$DBHOSTNAME
wp core install --url=https://wp.local.pcfdev.io --title="Wordpress on CloudFoundry" --admin_user=admin --admin_email=foo@bar.com --skip-email --admin_password=pass
# wp core language activate en_US
# wp plugin install flamingo
# wp plugin install google-analytics-for-wordpress
# wp plugin install wordpress-seo
# wp plugin install contact-form-7

popd
cf push 
