# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""WordPress Extension

Downloads, installs and configures WordPress
"""
import os
import json
import os.path
import logging

_log = logging.getLogger('wordpress')

def enable_sshfs(ctx):
    volume_dir = json.loads(os.environ['VCAP_SERVICES']).get('local-volume')[0].get('volume_mounts')[0].get('container_dir')
    cmds = []
    # Surprisingly $HOME is set to /home/vcap/app, not to /home/vcap for whatever reason. 
    # After "cf ssh wp" $HOME is set to /home/vcap. 
    # Confusing. 
    cmds.append(('mv', '/home/vcap/app//htdocs', volume_dir))
    cmds.append(('rm -rf', '/home/vcap/app/htdocs')) # in case the mv command failed
    cmds.append(('ln -s', '%s/htdocs' % volume_dir, '$HOME/'))
    _log.info(cmds)
    return cmds

# Extension Methods
def preprocess_commands(ctx):
    return enable_sshfs(ctx)

def service_commands(ctx):
    return {}

def service_environment(ctx):
    return {}

def compile(install):
    return 0
